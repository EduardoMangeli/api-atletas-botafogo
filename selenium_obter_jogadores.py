import json

from selenium import webdriver
from selenium.webdriver.common.by import By


def monta_jogadores(lista, elenco='masculino', id_jogador=1):
    jogadores = driver.find_elements(By.CSS_SELECTOR, '.elenco-destaque-slider .swiper-slide')
    for jogador in jogadores:
        descricao = jogador.find_element(By.CSS_SELECTOR, 'div.pr-4')
        detalhes = jogador.find_element(By.TAG_NAME, 'a').get_attribute('href')
        lista.append({
            'id': id_jogador,
            'url_detalhes': detalhes,
            'elenco': elenco,
            'imagem': jogador.find_element(By.TAG_NAME, 'img').get_attribute('src'),
            'n_jogos': jogador.find_element(By.CSS_SELECTOR, 'span:nth-child(2)').text.strip(),
            'nome': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(1)').text.strip(),
            'posicao': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(2)').text.split(':')[1].strip(),
            'naturalidade': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(3)').text.split(':')[1].strip(),
            'nascimento': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(4)').text.split(':')[1].strip(),
            'altura': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(5)').text.split(':')[1].strip(),
            'no_botafogo_desde': descricao.find_element(By.CSS_SELECTOR, 'span:nth-child(7)').text.split('desde ')[1].strip()
        })
        id_jogador += 1
    return id_jogador

def inclui_descricao(lista, browserdriver):
    for atleta in lista:
        browserdriver.get(atleta.get('url_detalhes'))

        atleta['detalhes'] = browserdriver.find_element(By.CSS_SELECTOR, 'h3~div').text

if __name__ == '__main__':
    try:
        driver = webdriver.Firefox()
        driver.implicitly_wait(2)
        driver.get('https://botafogo.com.br')

        versao = '2024-1'

        # lista_jogadores = []
        #
        # # masculino
        # monta_jogadores(lista_jogadores)
        #
        # # aceita cookies
        # driver.find_element(By.CSS_SELECTOR, "[aria-label='Accept cookies']").click()
        #
        # # feminino
        # driver.find_element(By.CSS_SELECTOR, 'button.btn-home-elenco:nth-child(2)').click() #exibe elenco feminino
        # monta_jogadores(lista_jogadores, elenco='feminino', id_jogador=len(lista_jogadores)+1)

        # inclui detalhes
        with open(f"jogadores{versao}.json", encoding='utf-8', mode='r') as f:
            lista_jogadores = json.load(f)
        inclui_descricao(lista_jogadores, driver)

        # cria arquivo
        with open(f"jogadores{versao}.json", encoding='utf-8', mode="w") as f:
            json.dump(lista_jogadores, f, ensure_ascii=False, indent=4)
    finally:
        driver.quit()

        print('terminou')
