import json

versao = '2024-1'

with open(f"jogadores{versao}.json", 'r') as f:
    lista_jogadores = json.load(f)

for atleta in lista_jogadores:
    atleta['imagem'] = f"https://botafogo-atletas.mange.li/static/{versao}/{atleta['id']}.png"

with open(f"jogadores{versao}.json", encoding='utf-8', mode='w') as f:
    json.dump(lista_jogadores, f, ensure_ascii=False, indent=4)
