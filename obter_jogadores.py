import requests
from bs4 import BeautifulSoup
import json

versao = '2024-1'

principal = requests.get("https://botafogo.com.br")

soup = BeautifulSoup(principal.content, "lxml")

jogadores = soup.select(".swiper-wrapper .swipper-slide")

lista_jogadores = []

indice_atleta = 1

for jogador in jogadores:

    if 'futebol-fem' in jogador.find_parent('section')['class']:
        elenco = 'feminino'
    else:
        elenco = 'masculino'

    detalhes = requests.get("https://botafogo.com.br/" + jogador.find('a')['href'])
    detalhes = BeautifulSoup(detalhes.content, 'html5lib')

    nome = jogador.find("h3").get_text()
    posicao = jogador.find("p").get_text()
    imagem = "https://botafogo.com.br" + jogador.find("img")['src']
    container_descricao = detalhes.select('div.col-desktop-lg-12:nth-child(2)')[0]

    if container_descricao.get('p'):
        descricao = container_descricao.get('p').get_text().strip()
    else:
        descricao = container_descricao.get_text().strip()

    nome_completo = detalhes.select_one(
        '.col-desktop-lg-9 > p:nth-child(1) > strong').next_sibling.get_text().strip(':').strip()

    nascimento = detalhes.select_one(
        '.col-desktop-lg-9 > p:nth-child(1) > strong:nth-child(3)').next_sibling.get_text().strip(':').strip()

    altura = detalhes.select_one(
        '.col-desktop-lg-9 > p:nth-child(1) > strong:nth-child(5)').next_sibling.get_text().strip(':').strip()

    obj = {
        'id': indice_atleta,
        'elenco': elenco,
        'nome': nome,
        'posicao': posicao,
        'imagem': imagem,
        'descricao': descricao,
        'nome_completo': nome_completo,
        'nascimento': nascimento,
        'altura': altura
    }

    indice_atleta += 1
    lista_jogadores.append(obj)

with open(f"jogadores{versao}.json", "w") as f:
    json.dump(lista_jogadores, f, ensure_ascii=False, indent=4)
