import json
from urllib.request import urlretrieve
import requests
from os import path
from pathlib import Path


versao = '2024-1'

with open(f"jogadores{versao}.json", "r") as f:
    dados = json.load(f)

Path(f"imagens/{versao}").mkdir(parents=True, exist_ok=True)

for atleta in dados:
    #urlretrieve(atleta.get('imagem'), filename=f"imagens/{versao}/{atleta.get('id')}.png")
    r = requests.get(atleta.get('imagem'))
    with open(f"imagens/{versao}/{atleta.get('id')}.png", 'wb') as f:
        f.write(r.content)