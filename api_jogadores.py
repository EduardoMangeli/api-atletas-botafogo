import base64

import uvicorn

from typing import Union

from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles

from fastapi.middleware.cors import CORSMiddleware

import json

app = FastAPI()

app.mount("/static", StaticFiles(directory="imagens"), name="static")

origins = ['*']

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

versao = '2024-1'

with open(f'jogadores{versao}.json', 'r') as f:
    jogadores = json.load(f)

@app.get(f"/{versao}/")
def read_root():
    return f"Api para listar atletas do Botafogo em {versao}"

@app.get(f"/{versao}/all")
def read_all():
    return jogadores

@app.get(f"/{versao}/feminino")
def read_feminino():
    return [x for x in jogadores if x['elenco'] == 'feminino']

@app.get(f"/{versao}/masculino")
def read_masculino():
    return [x for x in jogadores if x['elenco'] == 'masculino']

@app.get(f"/{versao}/{{atleta_id}}")
def read_atleta(atleta_id : int):
    try:
        return [x for x in jogadores if x['id'] == atleta_id][0]
    except IndexError:
        return f"Não há atleta com o id {atleta_id}."


#versão antiga

versao_old = '2023-2-2'

with open(f'jogadores{versao_old}.json', 'r') as f:
    jogadores_old = json.load(f)

@app.get('/')
def read_root():
    return f"Api para listar atletas do Botafogo. Veja /docs para conhecer todos os endpoints."

@app.get('/all')
def read_all():
    return jogadores_old

@app.get('/feminino')
def read_feminino():
    return [x for x in jogadores_old if x['elenco'] == 'feminino']

@app.get('/masculino')
def read_masculino():
    return [x for x in jogadores_old if x['elenco'] == 'masculino']

@app.get('/{atleta_id}')
def read_atleta(atleta_id : int):
    try:
        return [x for x in jogadores_old if x['id'] == atleta_id][0]
    except IndexError:
        return f"Não há atleta com o id {atleta_id}."


# @app.middleware('http')
# async def filtro(request: Request, call_next):
#     if request.url.path == "/all":
#         try:
#             code = bytes(request.headers['Authorization'].split()[1], encoding='utf-8')
#             code_string = base64.b64decode(code)
#             user, password = str(code_string).split(':')
#             if str(user) == 'user' and str(password) == 'joga muito':
#                 response = await call_next(request)
#             response = JSONResponse('Acesso Negado.', status_code=403)
#         except Exception as e:
#             response = JSONResponse(str(e), status_code=500)
#
#     else:
#         response = await call_next(request)
#     return response

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8003)
