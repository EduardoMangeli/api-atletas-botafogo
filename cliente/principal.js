//const url = 'https://botafogo-atletas.mange.li'

const url = 'http://localhost:8003'
const getTodos = async () => {
    const response = await fetch(`${url}/all`);
    const atletas = await response.json();
    return atletas;
}

const getElenco = async (elenco = 'masculino') => {
    const response = await fetch(`${url}/${elenco}`);
    const atletas = await response.json();
    return atletas;
}

const getAtleta = async (atleta) => {
     const response = await fetch(`${url}/${atleta}`);
    const atletas = await response.json();
    return atletas;
}

/*
// testando as funções

getTodos().then((dados) => console.log(dados));

getElenco('feminino').then((dados) => console.log(dados));

getAtleta(2).then((dados) => console.log(dados));

*/

const body = document.body;

const div_botoes = document.createElement('div')
const botao_todos = document.createElement('button')

botao_todos.onclick = () => getTodos().then((dados) => console.log(dados))
botao_todos.innerText = 'Todos'

div_botoes.appendChild(botao_todos)



body.appendChild(div_botoes)